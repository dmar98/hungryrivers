﻿using TMPro;
using UnityEngine;

namespace BunnyGame.Scripts.Display
{
	public class HighScoreDisplay : MonoBehaviour
	{
		public TextMeshProUGUI rank;
		public TextMeshProUGUI teamName;
		public TextMeshProUGUI score;

		private RectTransform _rectTransform;
	
		private void Start()
		{
			_rectTransform = GetComponent<RectTransform>();
		
			_rectTransform.anchorMin = new Vector2(0, 1);
			_rectTransform.anchorMax = new Vector2(1, 1);
			_rectTransform.pivot = new Vector2(0.5f, 1);
		
			_rectTransform.sizeDelta = new Vector2(900, 30.5f);
		}
	}
}
