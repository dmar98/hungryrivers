﻿using BunnyGame.Scripts.Static;
using TMPro;
using UnityEngine;

namespace BunnyGame.Scripts.Display
{
    public class ScoreDisplay : MonoBehaviour
    {
        private TextMeshProUGUI _scoreText;

        private void Start()
        {
            _scoreText = GetComponent<TextMeshProUGUI>();
        }

        private void Update()
        {
            _scoreText.text = "Score " + GameState.GameScore;
        }
    }
}
