﻿using BunnyGame.Scripts.Static;
using UnityEngine;
using UnityEngine.UI;

namespace BunnyGame.Scripts.Bunny
{
    public class PlayerOne : PlayerBase
    {
        public static bool AboutToRotate;
     
        private PlayerOne()
        {
            keyMappings = new KeyMappings
            {
                axisHorizontal = "Horizontal1",
                axisVertical = "Vertical1",
                rotateClockwise =  KeyCode.Alpha5,
                rotateCounterClockwise = KeyCode.Alpha6,
                usePowerUp = KeyCode.Alpha7
            };

            playerInput = new PlayerInput
            {
                input = Vector2.zero,
                oldInput = Vector2.zero
            };
            
            gameArea = new GameArea
            {
                pivot = new Vector2(0.5f, 0.5f), 
                spawn = new GameArea.Spawn
                {
                    position = Vector2.zero,
                    rotation = new Quaternion(0,0,0,1)
                }
            };

            powerUp = new PowerUp
            {
                ready = false,
                timer = 0,
                resetTime = 60,
                inProgress = false,
                executingTime = 0
            };

            isHorizontal = false;
        }

        #region Unity Methods

        protected override void Awake()
        {
            base.Awake();
            BaseFallSpeed = Screen.height * 0.375f * Gravity;
            BaseHorizontalSpeed = Screen.height * 0.25f;
            gameArea.spawn.position = new Vector2(Screen.width * 0.5f, 0);
        }

        protected override void Start()
        {
            base.Start();            
            AboutToRotate = false;
            powerUp.timer = GameState.CooldownOne;
            powerUpIcon = Resources.Load<Sprite>("Sprites/Bunnies/Player1");
            powerUpIconActive = Resources.Load<Sprite>("Sprites/Bunnies/Player1Active");
            powerUpImage = GameObject.FindWithTag("PowerUpCooldown1").GetComponent<Image>();
        }      

        protected override void Update()
        {
            base.Update();           
            
            if (!AboutToRotate)
            {
                return;
            }
            DeletePieces();

            powerUp.inProgress = false;
            powerUp.timer = powerUp.resetTime;

            switch (GameState.PlayerAmount)
            {
                case 2:
                    GamePieceSpawner.SpawnFood(Players.PlayerOne);
                    GamePieceSpawner.SpawnFood(Players.PlayerTwo);
                    break;
                
                case 4:
                    GamePieceSpawner.SpawnFood(Players.PlayerOne);
                    GamePieceSpawner.SpawnFood(Players.PlayerTwo);
                    GamePieceSpawner.SpawnFood(Players.PlayerThree);
                    GamePieceSpawner.SpawnFood(Players.PlayerFour);
                    break;
                
                default:
                    GamePieceSpawner.SpawnFood(Players.PlayerOne);
                    GamePieceSpawner.SpawnFood(Players.PlayerTwo);
                    GamePieceSpawner.SpawnFood(Players.PlayerThree);
                    GamePieceSpawner.SpawnFood(Players.PlayerFour);
                    break;
            }
            
            AboutToRotate = false;
        }

        private void DeletePieces()
        {
            foreach (Transform gamePiece in Players.PlayerOne.spawnContainer)
            {
                Destroy(gamePiece.gameObject);
            }
           
            foreach (Transform gamePiece in Players.PlayerTwo.spawnContainer)
            {
                Destroy(gamePiece.gameObject);
            }

            foreach (Transform gamePiece in Players.PlayerThree.spawnContainer)
            {
                Destroy(gamePiece.gameObject);
            }

            foreach (Transform gamePiece in Players.PlayerFour.spawnContainer)
            {
                Destroy(gamePiece.gameObject);
            }
        }

        protected override void OnValidate()
        {
        }
        
        #endregion

        #region Overrides

        protected override void Fall()
        {          
            InputChange(new Vector2(0, GamePiece.GetVelocity().y), new Vector2(GamePiece.GetVelocity().x, VerticalMultiplier * BaseFallSpeed * Time.deltaTime));
            GamePiece.ApplyForce(new Vector2(playerInput.input.x * BaseHorizontalSpeed, VerticalMultiplier * BaseFallSpeed * GameState.CurrentFallSpeedMultiplier * Time.deltaTime));
        }

        protected override void UsePowerUp()
        {
            AboutToRotate = true;  
        }

        #endregion
    }
}