﻿using BunnyGame.Scripts.Gameplay;
using BunnyGame.Scripts.Static;
using UnityEngine;
using UnityEngine.UI;

namespace BunnyGame.Scripts.Bunny
{
    public class PlayerFour : PlayerBase
    {
        private Vector2 _oldSize;
        private Vector2 _newSize;
        
        
        private PlayerFour()
        {
            keyMappings = new KeyMappings
            {
                axisHorizontal = "Horizontal4",
                axisVertical = "Vertical4",
                rotateClockwise =  KeyCode.B,
                rotateCounterClockwise = KeyCode.N,
                usePowerUp =  KeyCode.M
            };

            playerInput = new PlayerInput
            {
                input = Vector2.zero,
                oldInput = Vector2.zero
            };
            
            gameArea = new GameArea
            {
                pivot = new Vector2(0.5f, 0.5f), 
                spawn = new GameArea.Spawn
                {
                    position = Vector2.zero,
                    rotation = new Quaternion(0, 0, 0.7071068f, 0.7071068f)
                }              
            };
 
            powerUp = new PowerUp
            {
                ready = false,
                timer = 0,
                resetTime = 15,
                inProgress = false,
                executingTime = 12
            };
            
            isHorizontal = true;
        }

        #region Unity Methods

        protected override void Awake()
        {
            base.Awake();
            BaseFallSpeed = Screen.height * 0.375f * Gravity;
            BaseHorizontalSpeed = Screen.height * 0.25f;
            gameArea.spawn.position = new Vector2(Screen.width * 0.5f + Screen.height * 0.5f, Screen.height * 0.5f);
        }

        protected override void Start()
        {
            base.Start();           
            _newSize = new Vector2(Screen.height * 0.6f , Screen.height * 0.6f);
            _oldSize = new Vector2(Screen.height * 0.8f , Screen.height * 0.8f);

            powerUp.timer = GameState.CooldownFour;
            powerUpIcon = Resources.Load<Sprite>("Sprites/Bunnies/Player4");
            powerUpIconActive = Resources.Load<Sprite>("Sprites/Bunnies/Player4Active");
            powerUpImage = GameObject.FindWithTag("PowerUpCooldown4").GetComponent<Image>();
        }

        protected override void Update()
        {
            base.Update();

            if (!powerUp.inProgress)
            {
                return;
            }
            
            
            
            if (powerUp.timeOfExecution < powerUp.executingTime)
            {
                GameObject.FindWithTag("Limit").GetComponent<BoxCollider2D>().offset = _newSize;
                powerUp.timeOfExecution += Time.unscaledDeltaTime;
                GameObject.FindWithTag("Limit").GetComponent<Limit>().Shrink();
                return;
            }

            powerUp.inProgress = false;                        
            powerUp.timer = powerUp.resetTime;
            
            GameObject.FindWithTag("Limit").GetComponent<RectTransform>().sizeDelta = _oldSize;
            GameObject.FindWithTag("Limit").GetComponent<Limit>().Reset();
        }

        protected override void OnValidate()
        {
        }

        #endregion

        #region Overrides

        protected override void Fall()
        {       
            InputChange(new Vector2(GamePiece.GetVelocity().x, 0), new Vector2(-VerticalMultiplier * BaseFallSpeed * Time.deltaTime, GamePiece.GetVelocity().y));
            
            GamePiece.ApplyForce(new Vector2(-VerticalMultiplier * BaseFallSpeed * GameState.CurrentFallSpeedMultiplier *  Time.deltaTime,
                playerInput.input.x * BaseHorizontalSpeed));
        }

        protected override void UsePowerUp(){}

        #endregion
    }
}