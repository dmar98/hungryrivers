﻿using BunnyGame.Scripts.Gameplay;
using UnityEngine;
using BunnyGame.Scripts.Static;
using UnityEngine.UI;

namespace BunnyGame.Scripts.Bunny
{
    [RequireComponent(typeof(AudioSource))]
    public abstract class PlayerBase : MonoBehaviour
    {
        #region Structs
        [System.Serializable]
        public struct KeyMappings
        {
            public string axisHorizontal;
            public string axisVertical;
            public KeyCode rotateClockwise;
            public KeyCode rotateCounterClockwise;
            public KeyCode usePowerUp;
        }

        [System.Serializable]
        public struct PlayerInput
        {
            public Vector2 input;
            public Vector2 oldInput;
        }

        [System.Serializable]
        public struct GameArea
        {
            [System.Serializable]
            public struct Spawn
            {
                public Vector2 position;
                public Quaternion rotation;
            }

            public Spawn spawn;
            
            public Vector2 pivot;
        }

        [System.Serializable]
        public struct PowerUp
        {
            public float timer;
            public float resetTime;
            public float executingTime;
            public float timeOfExecution;
            public bool ready;
            public bool inProgress;
        }

        #endregion

        #region Variables

        //Structs.
        public KeyMappings keyMappings;
        public PlayerInput playerInput;
        public GameArea gameArea;
        public PowerUp powerUp;

        //Game Piece to be controlled.
        public GamePiece GamePiece { get; private set; }

        protected const float Gravity = 9.81f;

        //Pre-calculated numbers.
        protected float BaseFallSpeed;
        protected float BaseHorizontalSpeed;

        private bool HasGamePiece { get; set; }
        protected float VerticalMultiplier { get; private set; }
        public float timeSinceLastSpawn;


        public bool isHorizontal;

        public Transform spawnContainer;
        private AudioSource _audioSource;


        public Image powerUpImage;
        
        public Sprite powerUpIcon;
        public Sprite powerUpIconActive;

        private float _powerUpTime;
        
        
        #endregion

        #region Unity Methods
        
        protected virtual void Awake(){}
        
        protected virtual void Start()
        {
            timeSinceLastSpawn = 0;
            
            powerUp.ready = false;
            powerUp.inProgress = false;
            powerUp.timer = powerUp.resetTime;

            _powerUpTime = 255 / powerUp.resetTime;
            GamePieceSpawner.SpawnFood(this);
        }
        
        protected virtual void Update()
        {
            GetPlayerInput();
            PowerUpCooldown();

            powerUpImage.sprite = powerUp.ready ? powerUpIconActive : powerUpIcon;

            powerUpImage.enabled = !powerUp.inProgress;


            if (!Players.PlayerTwo.powerUp.inProgress)
            {
                timeSinceLastSpawn += Time.deltaTime;
            }

            if (!HasGamePiece)
            {
                return;
            }
            
            if (Input.GetKey(keyMappings.rotateClockwise))
            {
                GamePiece.transform.localEulerAngles += new Vector3(0, 0, -180) * Time.deltaTime;
            }
            
            if (Input.GetKey(keyMappings.rotateCounterClockwise))
            {
                GamePiece.transform.localEulerAngles += new Vector3(0, 0, 180) * Time.deltaTime;
            }
        }
        
        private void FixedUpdate()
        {
            if (ReferenceEquals(GamePiece, null))
            {
                HasGamePiece = false;
                return;
            }

            HasGamePiece = true;
            
            Fall();

            playerInput.oldInput.x = playerInput.input.x;
            playerInput.oldInput.y = playerInput.input.y;
        }

        #endregion

        #region Abstract
        
        protected abstract void Fall();
        protected abstract void UsePowerUp();
        protected abstract void OnValidate();

        #endregion
        
        protected void InputChange(Vector2 horizontal, Vector2 vertical)
        {
            if (Mathf.Abs(playerInput.oldInput.x - playerInput.input.x) > 0.01f)
            {
                GamePiece.SetVelocity(horizontal);               
            }
            
            if (Mathf.Abs(playerInput.oldInput.y - playerInput.input.y) > 0.01f)
            {
                GamePiece.SetVelocity(vertical);
            }
        }
        
        private void GetPlayerInput()
        {
            playerInput.input.x = Input.GetAxisRaw(keyMappings.axisHorizontal);
            playerInput.input.y = Input.GetAxisRaw(keyMappings.axisVertical);

            if (Input.GetKeyDown(keyMappings.usePowerUp) && powerUp.ready)
            {
                powerUp.ready = false;
                powerUp.inProgress = true;
                powerUp.timeOfExecution = 0;
                UsePowerUp();
            }

            VerticalMultiplier = playerInput.input.y >= 0.5f ? 4 : 1;           
        }

        #region Setter

        public void AssignGamePiece(GamePiece gamePiece)
        {
            GamePiece = gamePiece;
            HasGamePiece = true;
        }

        public void RemoveGamePiece()
        {
            GamePiece = null;
            HasGamePiece = false;
        }

        private void PowerUpCooldown()
        {
            var color = powerUpImage.color;
            color.a = (powerUp.resetTime - powerUp.timer) * _powerUpTime / 255;

            powerUpImage.color = color;
            
            
            
            if (powerUp.inProgress)
            {
                return;
            }

            if (powerUp.timer <= 0)
            {
                powerUp.ready = true;
                return;
            }

            powerUp.timer -= Time.deltaTime;
            powerUp.ready = false;
        }
        
        #endregion
    }
}