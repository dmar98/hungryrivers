﻿using BunnyGame.Scripts.Bunny;
using BunnyGame.Scripts.Static;
using UnityEngine;

namespace BunnyGame.Scripts.Gameplay
{
    public class Island : MonoBehaviour
    {
        private static RectTransform _rectTransform;

        private void OnCollisionEnter2D(Collision2D other)
        {
            if (!other.gameObject.CompareTag("GamePiece"))
            {
                return;
            }

            var gamePiece = other.gameObject.GetComponent<GamePiece>();

            if (gamePiece.hasCollidedOnce)
            {
                return;
            }

            gamePiece.hasCollidedOnce = true;
            GameState.GameScore += gamePiece.points * GameState.CurrentScoreMultiplier;
            gamePiece.player.RemoveGamePiece();
            gamePiece.collisionCollider.sharedMaterial = gamePiece.friction;
      
            if (PlayerOne.AboutToRotate)
            {
                return;
            }
            
            GamePieceSpawner.SpawnFood(gamePiece.player); 
        }
    }
}