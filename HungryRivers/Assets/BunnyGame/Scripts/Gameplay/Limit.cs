﻿using UnityEngine;

namespace BunnyGame.Scripts.Gameplay
{
    public class Limit : MonoBehaviour
    {
        private RectTransform _rectTransform;
        public BoxCollider2D boxCollider;




        public Animator animator;
        private static readonly int Shrink1 = Animator.StringToHash("Shrink");
        private static readonly int Grow = Animator.StringToHash("Grow");


        private void Start()
        {
            var size = new Vector2(756, 756);

            
            animator = GetComponent<Animator>();
            animator.SetBool(Shrink1, false);
            animator.SetBool(Grow, false);
            
            _rectTransform = GetComponent<RectTransform>();
            boxCollider = GetComponent<BoxCollider2D>();
        
            _rectTransform.sizeDelta = size;
            boxCollider.size = size;
            boxCollider.offset = Vector2.zero;
            
            Reset();
        }
    
        private void OnTriggerEnter2D(Collider2D other)
        {
            var gamePiece = other.GetComponent<GamePiece>();

            if (ReferenceEquals(gamePiece, null))
            {
                return;
            }

            gamePiece.insideLimit = true;
        }

        private void OnTriggerExit2D(Collider2D other)
        {
            var gamePiece = other.GetComponent<GamePiece>();

            if (ReferenceEquals(gamePiece, null))
            {
                return;
            }

            gamePiece.insideLimit = false;
        }

        public void Shrink()
        {
            animator.SetBool(Shrink1, true);
            animator.SetBool(Grow, false);
            boxCollider.size = new Vector2(453.6f, 453.6f);
            boxCollider.offset = Vector2.zero;
        }

        public void Reset()
        {
            animator.SetBool(Shrink1, false);
            animator.SetBool(Grow, true);
            boxCollider.size = new Vector2(720, 720);
        }
    }
}