﻿using System;
using TMPro;
using UnityEngine;

namespace BunnyGame.Scripts.Team_Name
{
    public class TeamLetter : MonoBehaviour
    {
        private TextMeshProUGUI _letterText;
        public char letter;

        public int asciiNumber;
    
        private void Awake()
        {
            _letterText = GetComponent<TextMeshProUGUI>();
            letter = 'A';
            asciiNumber = letter;
        }
    
        public void NextLetter()
        {
            asciiNumber++;

            if (asciiNumber == 96)
            {
                asciiNumber = 65;
            }
        
            if (asciiNumber == 91)
            {
                asciiNumber = 48;
            }

            if (asciiNumber == 58)
            {
                asciiNumber = 95;
            }

            letter = Convert.ToChar(asciiNumber);
            _letterText.text = Convert.ToChar(asciiNumber).ToString();
        }

        public void PreviousLetter()
        {
            asciiNumber--;
      
            if (asciiNumber == 64)
            {
                asciiNumber = 95;
            }
        
            if (asciiNumber == 94)
            {
                asciiNumber = 57;
            }

            if (asciiNumber == 47)
            {
                asciiNumber = 90;
            }
        
            letter = Convert.ToChar(asciiNumber);
            _letterText.text = Convert.ToChar(asciiNumber).ToString();
        }

        public void ChangeColor(Color32 color)
        {
            _letterText.color = color;
        }
    }
}