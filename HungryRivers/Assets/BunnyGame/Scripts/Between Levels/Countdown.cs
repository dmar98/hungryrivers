﻿using System.Collections;
using BunnyGame.Scripts.Static;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace BunnyGame.Scripts.Between_Levels
{
    public class Countdown : MonoBehaviour
    {
        public TextMeshProUGUI timeDisplay;

        public AudioSource source;
        public AudioClip complete;
    
        private void Start()
        {
            timeDisplay.text =  "";
            StartCoroutine(Show());
        }

        private IEnumerator Show()
        {        
            if (GameState.CurrentLevel == 1)
            {
                for (var i = 3; i > 0; i--)
                {
                    timeDisplay.text = timeDisplay.text = "Level 1 starts in " + i;
                    yield return new WaitForSeconds(1f);
                }
            
                timeDisplay.text = "Go!!!";
                yield return new WaitForSeconds(1);
                SceneManager.LoadScene("Game");
            }
            else
            { 
                source.PlayOneShot(complete);

                timeDisplay.text =  "Level " + (GameState.CurrentLevel - 1) + " complete!";
            

                yield return new WaitForSeconds(1);
            
                timeDisplay.text = "Your score is: " + GameState.GameScore;
            
                yield return new WaitForSeconds(2);
            
            
                for (var i = 3; i > 0; i--)
                {
                    timeDisplay.text = timeDisplay.text = "Level " + (GameState.CurrentLevel) + " starts in " + i;
                    yield return new WaitForSeconds(1f);
                }
            
                timeDisplay.text = "Go!!!";
                yield return new WaitForSeconds(1);
            
                SceneManager.LoadScene("Game");
            }
        }
    }
}